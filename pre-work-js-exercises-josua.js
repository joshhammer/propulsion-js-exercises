// 1.

function reverse(string) {
    return string.split('').reverse().join('');
}

console.log(reverse('Josua'));

// 2. 

function factorial(num){
    let result = 1;
    if(num >= 0){
        for(let i = 1; i <= num; i++){
            result *= i;
        }
    
        return result;
    }
    else {
        console.log('Number must be >= 0');
    }
}

console.log(factorial(5));

// 3.

function longest_word(sentence) {
    const splitSentence = sentence.split(' ');
    const sortedWords = splitSentence.sort((a,b)=> a.length > b.length ? -1 : 1)
    return sortedWords[0];
}

console.log(longest_word('What\'s the most important thing in life ?'));

// 4.

function sumNums(num) {
    let result = 0;
    for(let i = 0; i <= num; i++){
        result += i;
    }
    return result;
}

console.log(sumNums(6));

// 5. 

function time_conversion(minutes) {
    const hours = ('0' + Math.floor(minutes/60)).slice(-2);
    const restMinutes = ('0' + minutes%60).slice(-2);

    return `${hours}:${restMinutes}`;
}

console.log(time_conversion(125));

// 6. 

function count_vowels(string) {
    let vowelCount = 0;
    for(let i = 0; i < string.length; i++){
        if (string[i] === 'a' || string[i] === 'e' || string[i] === 'i' || string[i] === 'o' || string[i] === 'u'){
            vowelCount += 1;
        }
    }
    return `Vowelcount = ${vowelCount}`;
}

console.log(count_vowels('basejumper'));

// 7.

function palindrome(string) {
    const reversedString = string.split('').reverse().join('');
    return string === reversedString ? true : false;
}

console.log(palindrome('racecar'));

// 8.

function nearby_az(string) {
    if (string.match(/az/) || string.match(/a.z/) || string.match(/a..z/)){
        return true;
    }
    else {
        return false;
    }
}

console.log(nearby_az('abbz'));

// 9.

function two_sum(numsArray) {
    let indexArray = [];
    for(let i = 0; i < numsArray.length - 2; i++){
        for(let jj = 1; jj < numsArray.length; jj++){
            if(numsArray[i] + numsArray[jj] == 0){
                indexArray.push([i, jj]);
            }
        }
    }
    return indexArray;
}

console.log(two_sum([1, 3, -1, 5, -3])) // [[0, 2], [1, 4]]


// 10.

function is_power_of_two(num) {
    let result = num/2;
    console.log(result)
    if(result > 2){
        return is_power_of_two(result)
    }
    else if(result === 2){
        return true;
    }
    else {
        return false;
    }
}

console.log(is_power_of_two(16));

// 11. 

function repeatString(str, num) {
    let concString = "";

    if (num < 0) {
        return "";
    }

    for (i = 1; i <= num; i++) {
        concString += str;
    }
    return concString;
}

console.log(repeatString('U suck ', 5));

// 12. 

function add_all(arr) {
    let result = 0;
    for(let i = 0; i <= arr[1]; i++){
        result += i;
    }
    return result;
}

console.log(add_all([1, 4]));

// 13.

function is_it_true(args) {
    return Boolean(args);
}

console.log(is_it_true(1));

// 14. 

function largest_of_four(arr) {
    let maxArray = [];
    for(let i = 0; i < arr.length; i++){
        const sorted = arr[i].sort((a,b)=> b - a)
        maxArray.push(sorted[0]);
    }
    return maxArray;
}

console.log(largest_of_four([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]));

// 15. 

function isAnagram(test, original) {
    const orderedTest = test.toLowerCase().split('').sort().join('');
    const orderedOg = original.toLowerCase().split('').sort().join('');
    return orderedTest == orderedOg;
};

console.log(isAnagram("Buckethead", "DeathCubeK"));